
** README

Personal dotfiles, managed using the [dotfiles utility](https://github.com/jbernard/dotfiles)
- Arch Linux stuff
- i3 display manager
- zsh configs
- bash/zsh convenience function
